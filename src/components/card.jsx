import { Button, Card, CardActions, CardContent, Typography } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'

export default function Cards({id, name, description, schedule}) {
    return (
        <Card style={{marginTop:'20px'}}>
            <CardContent>
                <div style={{display:'flex', justifyContent:'space-between'}}>
                    <Typography gutterBottom variant="h5" component="div">
                        {name}
                    </Typography>
                    <Typography gutterBottom variant="body2" component="div">
                        {schedule}
                    </Typography>
                </div>
                <Typography variant="body2" color="text.secondary">
                    {description}
                </Typography>
            </CardContent>
            {/* <CardActions>
                <Link to={`/detail/${id}`}>
                    <Button size="small">Detail</Button>
                </Link>
            </CardActions> */}
        </Card>
    )
}
