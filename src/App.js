import './App.css';
import { Container } from '@mui/material';
import React from 'react';
import { Routes, Route } from "react-router-dom";
import Home from './pages/home';
import Detail from './pages/detail';

function App() {
  return (
    <Container maxWidth="sm" className='App-header'>
      <Routes>
        <Route path="/" element={<Home />} exact />
        <Route path="/detail/:id" element={<Detail />} />
      </Routes>
    </Container>
  );
}

export default App;
