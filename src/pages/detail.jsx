import { Typography } from "@mui/material"
import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"

export default function Detail() {
    const { id } = useParams()
    const [data, setData] = useState(null)
  
    const getDetail = async () => {
      const res = await axios.get(`http://localhost:8000/api/restaurant/${id}`).then(res => {
        const result = res.data.data
        setData(result)
      }).catch(err => console.log(err))
      return res
    }
  
    useEffect(() => {
      getDetail()
    }, [])
  
  
    return (
      <>
        <Typography variant="h3" style={{ textAlign: 'center', marginBottom:'20px' }}>
          Detail Restaurant
        </Typography>
        <Typography variant='h6'>Nama Resto</Typography>
        <Typography>{data?.name}</Typography>
        <br></br>
        <Typography variant='h6'>Deskripsi Resto</Typography>
        <Typography>{data?.description}</Typography>
        <br></br>
        <Typography variant='h6'>Jadwal Resto</Typography>
        <Typography>{data?.schedule}</Typography>
      </>
    )
  }