import Cards from "../components/card";
import '../App.css';
import { Button, Container, IconButton, TextField, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import SearchIcon from '@mui/icons-material/Search';

export default function Home() {
    const [data, setData] = useState([]);
    const [searchInput, setSearchInput] = useState('')


    const getData = async () => {
        const res = await axios.get('http://localhost:8000/api/restaurant').then((response) => {
            const result = response.data
            setData(result.data);
        });
        return res
    }

    const getSearch = async () => {
        const res = await axios.get(`http://localhost:8000/api/restaurant?search=${searchInput}`).then((response) => {
            const result = response.data
            setData(result.data)
        })
        return res
    }

    const handleSearch = (e) => {
        setSearchInput(e)
    }

    useEffect(() => {
        getData()
    }, []);

    return (
        <React.Fragment>
            <Typography variant="h3" style={{ textAlign: 'center' }}>
                Restaurant List
            </Typography>
            <TextField fullWidth id="outlined-basic" label="Cari nama atau jam buka" variant="outlined" onChange={(e) => handleSearch(e.target.value)} />
            <Button fullWidth variant="outlined" onClick={()=>getSearch()}>cari</Button>
            {
                data?.rows.map(res => (
                    <Cards key={res.id} name={res.name} description={res.description} schedule={res.schedule} />
                )) 
            }
        </React.Fragment>
    );
}
